angular
.module('app')
.config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {
  $stateProvider
  // .state('app.icons', {
  //   url: "/icons",
  //   abstract: true,
  //   template: '<ui-view></ui-view>',
  //   ncyBreadcrumb: {
  //     label: 'Icons'
  //   }
  // })
  // .state('app.icons.fontawesome', {
  //   url: '/font-awesome',
  //   templateUrl: 'views/icons/font-awesome.html',
  //   ncyBreadcrumb: {
  //     label: 'Font Awesome'
  //   }
  // })
  // .state('app.icons.simplelineicons', {
  //   url: '/simple-line-icons',
  //   templateUrl: 'views/icons/simple-line-icons.html',
  //   ncyBreadcrumb: {
  //     label: 'Simple Line Icons'
  //   }
  // })
  .state('app.users', {
    url: "/capacitaciones",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Capacitaciones'
    }
  })

  .state('app.users.admin', {
    url: '/gestion',
    templateUrl: 'views/users/userlist.html',
    ncyBreadcrumb: {
      label: 'Gestion'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/UsersCtrl.js']
        });
      }]
    }
  })
  .state('app.perfil', {
    url: '/perfil',
    templateUrl: 'views/pages/perfil.html',
    ncyBreadcrumb: {
      label: 'Perfil'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/PerfilCtrl.js']
        });
      }]
    }
  })
  .state('app.dispositives', {
    url: "/dispositivos",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Dispositivos'
    }
  })
  .state('app.dispositives.list', {
    url: '/gestion',
    templateUrl: 'views/dispositives/dispositiveslist.html',
    ncyBreadcrumb: {
      label: 'Gestion'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/DispositivesCtrl.js']
        });
      }]
    }
  })
  .state('app.microcontrollers', {
    url: "/instructores",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Instructores'
    }
  })
  .state('app.microcontrollers.list', {
    url: '/gestion',
    templateUrl: 'views/microcontrollers/microlist.html',
    ncyBreadcrumb: {
      label: 'Gestion'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/MicrocontrollersCtrl.js']
        });
      }]
    }
  })
  .state('app.actions', {
    url: "/historicos",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Historicos'
    }
  })
  .state('app.actions.list', {
    url: '/consultar',
    templateUrl: 'views/actions/actionslist.html',
    ncyBreadcrumb: {
      label: 'Lista'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/ActiRemoCtrl.js']
        });
      }]
    }
  })
  .state('app.snmp', {
    url: "/planificacion",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Planificacion'
    }
  })
  .state('app.snmp.list', {
    url: '/detalles',
    templateUrl: 'views/snmp/snmplist.html',
    ncyBreadcrumb: {
      label: 'Detalles'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/SnmpCtrl.js']
        });
      }]
    }
  })
  .state('app.snmp.asign', {
    url: '/asignar',
    templateUrl: 'views/snmp/snmpasign.html',
    ncyBreadcrumb: {
      label: 'Asignar'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/SnmpCtrl.js']
        });
      }]
    }
  })
  .state('app.status', {
    url: "/estadisticas",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Estadísticas'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/StatusCtrl.js', 'js/controllers/charts.js']
        });
      }]
    }
  })
  .state('app.status.red', {
    url: '/rendimiento',
    templateUrl: 'views/status/red.html',
    ncyBreadcrumb: {
      label: 'Rendimiento'
    }
  })
  .state('app.status.dispositive', {
    url: '/dispositivo',
    templateUrl: 'views/status/dispositive.html',
    ncyBreadcrumb: {
      label: 'Dispositivo'
    }
  })
  .state('app.stadistics', {
    url: '/estadisticas',
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Estadísticas'
    }
  })
  .state('app.stadistics.show', {
    url: '/graficas',
    templateUrl: 'views/stadistics/stadistics.html',
    ncyBreadcrumb: {
      label: 'Gráficas'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/charts.js']
        });
      }]
    }
  })
  .state('app.reports', {
    url: "/reportes",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Reportes'
    }
  })
  .state('app.reports.list', {
    url: '/consultar',
    templateUrl: 'views/reports/reportslist.html',
    ncyBreadcrumb: {
      label: 'Lista'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/RepoCtrl.js']
        });
      }]
    }
  })
  .state('app.reports.structure', {
    url: '/estructura',
    templateUrl: 'views/reports/reportstructure.html',
    ncyBreadcrumb: {
      label: 'Estructura'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/RepoCtrl.js']
        });
      }]
    }
  })
  .state('app.student', {
    url: "/estudiante",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Estudiantes'
    }
  })
  .state('app.student.evaluation', {
    url: '/evaluacion',
    templateUrl: 'views/studentsAndInstructor/evaluation.html',
    ncyBreadcrumb: {
      label: 'Realizar Evaluación'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/evaluation.js']
        });
      }]
    }
  })
  .state('app.student.courses', {
    url: '/cursos',
    templateUrl: 'views/studentsAndInstructor/coursesMade.html',
    ncyBreadcrumb: {
      label: 'Cursos Realizados'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/coursesMade.js']
        });
      }]
    }
  })
  .state('app.instructor', {
    url: "/instructor",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Instructor'
    }
  })
  .state('app.instructor.execute', {
    url: '/ejecutar',
    templateUrl: 'views/studentsAndInstructor/executeEvaluation.html',
    ncyBreadcrumb: {
      label: 'Ejecutar Evaluación'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/executeEvaluation.js']
        });
      }]
    }
  })

  // .state('app.components.social-buttons', {
  //   url: '/social-buttons',
  //   templateUrl: 'views/components/social-buttons.html',
  //   ncyBreadcrumb: {
  //     label: 'Social Buttons'
  //   }
  // })
  // .state('app.components.cards', {
  //   url: '/cards',
  //   templateUrl: 'views/components/cards.html',
  //   ncyBreadcrumb: {
  //     label: 'Cards'
  //   }
  // })
  // .state('app.components.forms', {
  //   url: '/forms',
  //   templateUrl: 'views/components/forms.html',
  //   ncyBreadcrumb: {
  //     label: 'Forms'
  //   }
  // })
  // .state('app.components.switches', {
  //   url: '/switches',
  //   templateUrl: 'views/components/switches.html',
  //   ncyBreadcrumb: {
  //     label: 'Switches'
  //   }
  // })
  // .state('app.components.tables', {
  //   url: '/tables',
  //   templateUrl: 'views/components/tables.html',
  //   ncyBreadcrumb: {
  //     label: 'Tables'
  //   }
  // })
  .state('app.widgets', {
    url: '/widgets',
    templateUrl: 'views/widgets.html',
    ncyBreadcrumb: {
      label: 'Widgets'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/widgets.js']
        });
      }]
    }
  })
  .state('app.charts', {
    url: '/charts',
    templateUrl: 'views/charts.html',
    ncyBreadcrumb: {
      label: 'Charts'
    },
    resolve: {
      // Plugins loaded before
      // loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
      //     return $ocLazyLoad.load([
      //         {
      //             serial: true,
      //             files: ['js/libs/Chart.min.js', 'js/libs/angular-chart.min.js']
      //         }
      //     ]);
      // }],
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/charts.js']
        });
      }]
    }
  })
}]);
