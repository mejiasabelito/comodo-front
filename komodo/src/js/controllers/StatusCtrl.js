angular
.module('app.status',[])
.controller('StatusCtrl', function ($scope, $uibModal) {
  
  $scope.open = function (size) {
    var modalInstance =   $uibModal.open({
      animation: false,
      controller: 'DisModalCtrl',
      templateUrl: 'views/actions/addActDis.html',
      scope: $scope,
      size: size,
      // resolve: {
      //   shift: function () {
      //     return {shift: shift, index: index};
      //   }
      // }
    });
    modalInstance.result.then(function () {
      // if(shift){
      //   if(index){
      //     angular.merge($scope.shifts[index], $scope.shifts[index], shift);
      //     angular.merge($scope.safeShifts[index], $scope.safeShifts[index], shift);
      //   }else{
      //     $scope.getShifts();
      //   }
      // }
    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  };

  $scope.open_micro = function (size) {
    var modalInstance =   $uibModal.open({
      animation: false,
      controller: 'DisModalCtrl',
      templateUrl: 'views/actions/addActMic.html',
      scope: $scope,
      size: size,
      // resolve: {
      //   shift: function () {
      //     return {shift: shift, index: index};
      //   }
      // }
    });
    modalInstance.result.then(function () {
      // if(shift){
      //   if(index){
      //     angular.merge($scope.shifts[index], $scope.shifts[index], shift);
      //     angular.merge($scope.safeShifts[index], $scope.safeShifts[index], shift);
      //   }else{
      //     $scope.getShifts();
      //   }
      // }
    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  };
});

angular.module('app.status').controller('DisModalCtrl', function ($uibModalInstance, $scope) {

  $scope.ok = function () {
    $uibModalInstance.close();
  };
  //
  // $ctrl.cancel = function () {
  //   $uibModalInstance.dismiss('cancel');
  // };
});
