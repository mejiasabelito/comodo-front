angular
.module('app.instructor.execute',[])
.controller('ExecuteCtrl', function ($scope, $uibModal, $webSql, toastr, $state) {


  $scope.readMicros = function () {
     $scope.microsData = [
        {
          "name": "Centinela",
          "status": "Activo"
        },
        {
          "name": "Drilling",
          "status": "Inactivo"
        },
        {
          "name": "OpenWell",
          "status": "Inactivo"
        },
     ];

  };
  $scope.executeTest = function(){
    toastr.success('Prueba Iniciada');
    $state.go('app.main');
  }
  $scope.cancel = function(){
    toastr.error('Prueba Cancelada');
    $state.go('app.main');
  }
})
