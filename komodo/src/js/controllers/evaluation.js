angular
.module('app.student.evaluation',[])
.controller('EvaluationCtrl', function ($scope, $uibModal, $webSql, toastr) {
  $scope.testStart = false;

  $scope.executeTest = function(){
    $scope.testStart = true;
  }
  $scope.sendAnswer = function(){
    $scope.microsData = [
       {
         "answer": "¿Cuál de los elementos es el mas usado por el método de Drilling?",
         "option": [

             "SAFG",
             "HDF",
             "Producción",
             "UFSE"

         ]
       }
    ];
  }

     $scope.microsData = [
        {
          "answer": "¿En cuanto tiempo se pasa del estado de puesta en perforación a Producción un Pozo?",
          "option": [

              "1 semana",
              "2 meses",
              "3 semanas",
              "6 semanas"

          ]
        }
     ];


})
