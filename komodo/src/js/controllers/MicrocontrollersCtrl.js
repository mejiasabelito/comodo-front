angular
.module('app.microcontrollers.list',[])
.controller('MicrocontrollersCtrl', function ($scope, $uibModal, $webSql, toastr) {

  $scope.db = $webSql.openDatabase('komodo', '1.0', 'Komodo DB', 2 * 1024 * 1024);

  $scope.readMicros = function () {
     $scope.microsData = [
        {
          "name": "Jose Martinez",
          "email": "jmartinez@PDVSA.com",
          "status": "Inactivo"
        },
        {
          "name": "Angel Rodriguez",
          "email": "arodriguez@PDVSA.com",
          "status": "Activo"
        },
        {
          "name": "Carlos Perez",
          "email": "cap@PDVSA.com",
          "status": "Inactivo"
        },
        {
          "name": "Ernesto Guzmán",
          "email": "egm@PDVSA.com",
          "status": "Activo"
        },
        {
          "name": "Rommel Guevara",
          "email": "rommel@PDVSA.com",
          "status": "Activo"
        },
     ];

  };
  $scope.changeStatus = function (index){
     if ($scope.microsData[index].status == 'Activo') {
       $scope.microsData[index].status = 'Inactivo'
     }else{
        $scope.microsData[index].status = 'Activo'
     }
  }

  $scope.open = function (size, micro) {
    var modalInstance =   $uibModal.open({
      animation: false,
      controller: 'microModalCtrl',
      templateUrl: 'views/microcontrollers/addMicro.html',
      scope: $scope,
      size: size,
       resolve: {
        micro: function () {
          return {micro: micro};
        }
       }
    });
    modalInstance.result.then(function () {
      // if(shift){
      //   if(index){
      //     angular.merge($scope.shifts[index], $scope.shifts[index], shift);
      //     angular.merge($scope.safeShifts[index], $scope.safeShifts[index], shift);
      //   }else{
      //     $scope.getShifts();
      //   }
      // }
    }, function (data) {
            if(data == "update"){
        $scope.readMicros();
      }
    });
  };

    $scope.delete = function (id) {
    var r= confirm('¿Realmente desea eliminar este instructor de la lista?');
    if(r){
      console.log("deleted id"+id)
      $scope.db.del("controllers", {"id": id});
      $scope.readMicros();
    }
  };

})
.controller('microModalCtrl', function ($uibModalInstance, $scope, $webSql, toastr, micro) {

  $scope.toastr = toastr;
  $scope.micro = {};

  if(angular.isDefined(micro.micro.id)){
     angular.copy(micro.micro, $scope.micro);
  }else{
    $scope.new = true;
    $scope.micro = { loop_time: '', status: '' };
  }
  $scope.db = $webSql.openDatabase('komodo', '1.0', 'Komodo DB', 2 * 1024 * 1024);

  $scope.addMicro = function () {
    $scope.db.insert('controllers', $scope.micro)
    .then(function(results) {
      console.log(results);
      $uibModalInstance.dismiss('update');
    },function (err) {
      if(err.code === 6){
        $scope.toastr.error("Valores duplicados de Microcontrolador");
      }
      console.log(err);
    });

  };


  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
