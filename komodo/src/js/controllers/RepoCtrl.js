angular
.module('app.reports.list',[])
.controller('RepoCtrl', function ($scope, $uibModal) {

  $scope.reportsOptions = {
    "date":true,
    "time":true,
    "device":true,
    "trace":true,
    "issue":true,
    "status":true,
    "snmpw":false,
    "snmpg":false,
    "commands":true
  };

  $scope.firstReport = {
    name: 'Reporte RP1002',
    device: 'Router Oficina 2',
    date: '28/02/2018 10:40',
    status: 'Pendiente',
    commands: [
      {
        command: 'snmpget',
        response: 'Fallido Sin respuesta'
      },{
        command: 'snmpwalk',
        response: 'Change 22'
      },{
        command: 'snmptranslate',
        response: 'Correcto'
      },{
        command: 'snmpbulkget',
        response: 'Correcto'
      }
    ],
    stacktrace: `
        snmpget0102 0617 11 0100004709
Ahorros Venezuela: Failure in sendto (Operation not permitted)
        SNMPv2-MIB::sysDescr.0 = HP-UX net-snmp B.10.20 A 9000/715
        sysUpTime OBJECT-TYPE
        -- FROM       SNMPv2-MIB, RFC1213-MIB
        SYNTAX        TimeTicks
        MAX-ACCESS    read-only
        STATUS        current
        DESCRIPTION   "The time (in hundredths of a second) since the network
                  management portion of the system was last re-initialized."
        ::= { iso(1) org(3) dod(6) internet(1) mgmt(2) mib-2(1) system(1) 3 }
        55.1.1.0 = 2
       55.1.2.0 = 64
       55.1.3.0 = Gauge32: 4
       55.1.4.0 = Counter32: 0
       55.1.5.1.1.1 = 1
       55.1.5.1.2.1 = "ns0"
       55.1.5.1.3.1 = OID: .ccitt.zeroDotZero
       55.1.5.1.4.1 = 1500
       55.1.5.1.5.1 = 65535
       55.1.5.1.6.1 = IpAddress: 00 00 00 00 00 00 00 00 02 05 00 FF FE 00 02 AB
       55.1.5.1.7.1 = 64
       55.1.5.1.8.1 =  Hex: 00 05 00 00 02 AB
       55.1.5.1.9.1 = 1
       55.1.5.1.10.1 = 1
    `
  };
  $scope.secondReport = {
    name: 'Reporte RP1003',
    device: 'Switch Sala de Servidores',
    date: '1/03/2018 00:35',
    status: 'Resuelto',
    commands: [
      {
        command: 'ping',
        response: 'Fallido Sin respuesta'
      },{
        command: 'snmpwalk',
        response: 'Fallido Sin respuesta'
      },{
        command: 'restart',
        response: 'Ejecutado Correctamente'
      },{
        command: 'snmpget',
        response: 'Correcto'
      },
    ],
    stacktrace: `
      PING 192.168.1.13 (192.168.1.13) 56(84) bytes of data.
      From 192.168.1.106 icmp_seq=1 Destination Host Unreachable
      snmpwalk: Failure in sendto (Operation not permitted)
      Hello From Komodo!
      SNMPv2-MIB::sysUpTime.0 = Timeticks: (586731977) 0 days, 00:5:39.77
    `
  };
  $scope.thirdReport = {
    name: 'Reporte RP1003',
    device: 'Router Sala de Servidores',
    date: '3/03/2018 14:20',
    status: 'Fallido',
    commands: [
      {
        command: 'snmpget',
        response: 'Fallido Sin respuesta'
      },{
        command: 'ping',
        response: 'Fallido sin respuesta'
      },{
        command: 'snmpget',
        response: 'Fallido Sin respuesta'
      }
    ],
    stacktrace: `
        snmpgeet: Failure in sendto (Operation not permitted)
        PING 192.168.1.118 (192.168.1.118) 56(84) bytes of data.
        From 192.168.1.106 icmp_seq=1 Destination Host Unreachable
        snmpgeet: Failure in sendto (Operation not permitted)
        Enviada Alerta a Administradores
    `
  };
  $scope.open = function (size, report) {
    var modalInstance =   $uibModal.open({
      animation: false,
      controller: 'reportDetailsModalController',
      templateUrl: 'views/reports/reportDetails.html',
      scope: $scope,
      size: size,
      resolve: {
        report: report
      }
    });

    modalInstance.result.then(function () {
      // if(shift){
      //   if(index){
      //     angular.merge($scope.shifts[index], $scope.shifts[index], shift);
      //     angular.merge($scope.safeShifts[index], $scope.safeShifts[index], shift);
      //   }else{
      //     $scope.getShifts();
      //   }
      // }
    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  };

})
.controller('reportDetailsModalController', function ($uibModalInstance, $scope, report) {

  $scope.report = report;

  $scope.ok = function () {
    $uibModalInstance.close();
  };
  //
  // $ctrl.cancel = function () {
  //   $uibModalInstance.dismiss('cancel');
  // };
});
