angular
.module('app.snmp.list',[])
.controller('SnmpCtrl', function ($scope, $uibModal, $webSql, toastr, $state) {
  $scope.course_generator = false;
  $scope.mostrar = "Mostrar"
  $scope.users = [
  {
    "first_name": "Annedys",
    "last_name": "Campos",
    "id": "12345678",
    "level": "Básico"
  },
  {
    "first_name": "Annedys",
    "last_name": "Campos",
    "id": "12345678",
    "level": "Básico"
  },
  {
    "first_name": "Annedys",
    "last_name": "Campos",
    "id": "12345678",
    "level": "Básico"
  },
  {
    "first_name": "Annedys",
    "last_name": "Campos",
    "id": "12345678",
    "level": "Básico"
  },
  {
    "first_name": "Annedys",
    "last_name": "Campos",
    "id": "12345678",
    "level": "Básico"
  },

  ]
  $scope.dataCourse = {
    "name": "Drilling",
    "start": "01/05/2018",
    "users": $scope.users,
    "evaluation": "02/05/2018",
    "refrigers": $scope.users.length,
    "content": $scope.users.length
  }
  $scope.mostrar_curso = function(curso){
    $scope.course_generator = !curso
    if ($scope.mostrar == "Mostrar") {
      $scope.mostrar = "Ocultar"
    }else{
      $scope.mostrar = "Mostrar"

    }
  }
  $scope.acceptPlan = function(){
    toastr.success("Planificación Aceptada con éxito");
    $state.go("app.main")
  }
  $scope.posposePlan= function(){
    toastr.warning("Planificación Pospuesta");
    $state.go("app.main")
  }
  $scope.db = $webSql.openDatabase('komodo', '1.0', 'Komodo DB', 2 * 1024 * 1024);

  $scope.readDevices = function () {
    $scope.db.selectAll("devices").then(function(results) {
      $scope.devicesData = [];
      for(var i=0; i < results.rows.length; i++){
        $scope.devicesData.push(results.rows.item(i));
      }
    });
  };

  $scope.readCommands = function () {
    $scope.db.selectAll("commands").then(function(results) {
      $scope.commandsData = [];
      for(var i=0; i < results.rows.length; i++){
        $scope.commandsData.push(results.rows.item(i));
      }
    });
  };

  $scope.getDeviceComand = function (id) {
    $scope.db.selectAll("device_command").then(function(results) {
      $scope.deviceCommand = [];
      for(var i=0; i < results.rows.length; i++){
        console.log(results.rows.item(i));
        $scope.deviceCommand.push(results.rows.item(i));
      }
    });
  };

  $scope.singleCommand = function (id) {
    var command;
    angular.forEach($scope.commandsData, function(value, key) {
      if(value.id == id){
        command = value;
      }
    })
    return command;
  };

  $scope.open = function (size) {
    var modalInstance =   $uibModal.open({
      animation: false,
      controller: 'addCmd',
      templateUrl: 'views/snmp/addcomando.html',
      scope: $scope,
      size: size,
      // resolve: {
      //   shift: function () {
      //     return {shift: shift, index: index};
      //   }
      // }
    });
    modalInstance.result.then(function () {

    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  };

})
.controller('SnmpAsingCtrl', function ($scope, $webSql, toastr) {

  $scope.assignement = {};

  $scope.db = $webSql.openDatabase('komodo', '1.0', 'Komodo DB', 2 * 1024 * 1024);

  $scope.readDevices = function () {
    $scope.db.selectAll("devices").then(function(results) {
      $scope.devicesData = [];
      for(var i=0; i < results.rows.length; i++){
        $scope.devicesData.push(results.rows.item(i));
      }
    });
  };

  $scope.readCommands = function () {
    $scope.db.selectAll("commands").then(function(results) {
      $scope.commandsData = [];
      for(var i=0; i < results.rows.length; i++){
        $scope.commandsData.push(results.rows.item(i));
      }
    });
  };

  $scope.ok = function() {
    angular.forEach($scope.assignement.commands, function (value, key) {
      $scope.db.insert('device_command', {command_id: value, device_id:$scope.assignement.dispositivo_id })
      .then(function(results) {
        $scope.assignement = {};
        console.log(results);
      },function (err) {
        if(err.code === 6){
          $scope.toastr.error("Comando duplicado");
        }
      });
    });
  };

  $scope.cancel = function() {
    $scope.assignement = {};
  };

})

.controller('addCmd', function ($uibModalInstance, $scope, $webSql, toastr) {

  $scope.toastr = toastr;
  $scope.commandData = {};
  $scope.db = $webSql.openDatabase('komodo', '1.0', 'Komodo DB', 2 * 1024 * 1024);

  $scope.addCommand = function () {
    $scope.db.insert('commands', $scope.commandData)
    .then(function(results) {
      console.log(results);
      $uibModalInstance.dismiss('update');
    },function (err) {
      if(err.code === 6){
        $scope.toastr.error("Comando duplicado");
      }
    });

  };
  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };

  $scope.ok = function () {
    $uibModalInstance.close();
  };

  // $ctrl.cancel = function () {
  //   $uibModalInstance.dismiss('cancel');
  // };
});
