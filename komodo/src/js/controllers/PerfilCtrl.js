angular
.module('app.users.admin',[])
.controller('PerfilCtrl', function ($scope, $uibModal, $webSql, toastr, $state) {

  //$scope.db = $webSql.openDatabase('komodo', '1.0', 'Komodo DB', 2 * 1024 * 1024);


  if ($scope.userType == 'admin') {
      $scope.dataPerfil = {
      "first_name": "Annedys",
      "last_name": "Campos",
      "id": "123456789",
      "user_type": "Administrador",
      "phone": "123456789",
      "cel_phone": "12345678",
      "indicator": "A-23-HY",
      "pdvsa_email": "a@PDVSA.com",
      "password": "ajsbdjkscnlswcbkwqjdhwkqjxbwkq",
      "avatar": "img/avatars/imagenmujer.png"
    }
  }else{
    if ($scope.userType == 'student') {

      $scope.dataPerfil = {
        "first_name": "Yonathan",
        "last_name": "Brito",
        "id": "123456789",
        "user_type": "Estudiante",
        "phone": "123456789",
        "cel_phone": "12345678",
        "indicator": "A-23-HY",
        "pdvsa_email": "y@PDVSA.com",
        "password": "ajsbdjkscnlswcbkwqjdhwkqjxbwkq",
        "avatar": "img/avatars/6.png"
      }
    }else{
      $scope.dataPerfil = {
        "first_name": "Rommel",
        "last_name": "Guevara",
        "id": "123456789",
        "user_type": "Instructor",
        "phone": "123456789",
        "cel_phone": "12345678",
        "indicator": "A-22-HY",
        "pdvsa_email": "r@PDVSA.com",
        "password": "ajsbdjkscnlswcbkwqjdhwkqjxbwkq",
        "avatar": "img/avatars/6.png"
      }
    }
  }

  $scope.savePerfil = function(){
    toastr.success('Datos Guardados con éxito');
    $state.go('app.main')
  }

  $scope.open = function (size, user) {
    var modalInstance =   $uibModal.open({
      animation: false,
      controller: 'ModalInstanceCtrl',
      templateUrl: 'views/users/addUser.html',
      scope: $scope,
      size: size,
      resolve: {
        user: function () {
          return {user: user};
        }
      }
    });
    modalInstance.result.then(function () {

    }, function (data) {
      console.log(data);
      if(data == "update"){
        $scope.readUsers();
      }

    });
  };


})
.controller('ModalInstanceCtrl', function ($uibModalInstance, $scope, $webSql, toastr, user, $state) {
  $scope.toastr = toastr;
  $scope.userData = {};

  if(angular.isDefined(user.user.ci)){
     angular.copy(user.user, $scope.userData);
  }else{
    $scope.userData = {document:"c", nationality:"v", type: "employee"};
    $scope.new = true;
  }

    $scope.savePerfil = function(){
      toastr.success('Datos Guardados con éxito');
      $state.go('app.main')
    }
  //$scope.db = $webSql.openDatabase('komodo', '1.0', 'Komodo DB', 2 * 1024 * 1024);




  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
