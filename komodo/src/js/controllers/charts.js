//chart.js
angular
.module('app')
.controller('LineCtrl', LineCtrl)
.controller('BarCtrl', BarCtrl)
.controller('DoughnutCtrl', DoughnutCtrl)
.controller('RadarCtrl', RadarCtrl)
.controller('PieCtrl', PieCtrl)
.controller('PolarAreaCtrl', PolarAreaCtrl)

LineCtrl.$inject = ['$scope'];
function LineCtrl($scope) {
  $(function () {
      $('#datetimepicker1').datetimepicker();
  });
  $scope.labels = ['Febrero', 'Marzo', 'Abril'];
  // $scope.series = ['Ancho Disponible', 'Ancho en Uso'];
  $scope.data = [
    [9, 8, 10],
    [8, 7, 9]
  ];
  $scope.data2 = [
    [6, 4, 8],
    [3, 2, 5]
  ];
  $scope.dataDis = [
    [8, 7, 9, 8, 6, 6, 7, 6, 8, 9, 7, 7],
    [2, 3, 4, 4, 1, 1, 3, 2, 4, 3, 2, 4]
  ];
  $scope.dataAva = [
    [100, 70 ,83]
  ];
  $scope.dataAva2 = [
    [84, 75 , 91]
  ];
  $scope.seriesAva = ['Disponibilidad']
}

BarCtrl.$inject = ['$scope'];
function BarCtrl($scope) {
  $scope.labels = ['2011', '2012', '2013', '2014', '2015', '2016', '2017'];
  $scope.series = ['Series A'];

  $scope.data = [
    [10, 15, 8, 10, 8, 10, 8]
  ];
}

DoughnutCtrl.$inject = ['$scope'];
function DoughnutCtrl($scope) {
  $scope.labels = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
  $scope.data = [300, 500, 100];
}

RadarCtrl.$inject = ['$scope'];
function RadarCtrl($scope) {
  $scope.labels =['Eating', 'Drinking', 'Sleeping', 'Designing', 'Coding', 'Cycling', 'Running'];

  $scope.data = [
    [65, 59, 90, 81, 56, 55, 40],
    [28, 48, 40, 19, 96, 27, 100]
  ];
}

PieCtrl.$inject = ['$scope'];
function PieCtrl($scope) {
  $scope.labels = ['Respuesta Automática', 'Respuesta Manual'];
  $scope.data = [2, 1];
  $scope.data2 = [9, 5];
}

PolarAreaCtrl.$inject = ['$scope'];
function PolarAreaCtrl($scope) {
  $scope.labels = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales', 'Tele Sales', 'Corporate Sales'];
  $scope.data = [300, 500, 100, 40, 120];
}
