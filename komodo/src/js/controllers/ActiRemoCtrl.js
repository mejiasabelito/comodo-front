angular
.module('app.actions.list',[])
.controller('ActiRemoCtrl', function ($scope, $uibModal, $webSql, toastr) {

  //$scope.db = $webSql.openDatabase('komodo', '1.0', 'Komodo DB', 2 * 1024 * 1024);

   $scope.devicesData = [
      {
        "action":"Curso de Centinela Pospuesto para aprobación",
        "detail": "Se pospuso la planificación realizada del curso de Centinela Avanzado",
        "date": "20/04/2018"
      },
       {
        "action":"Nuevo Curso creado",
        "detail": "Curso de Drilling planificado",
        "date": "20/03/2018"
      },
       {
        "action":"Lista de posibles instructores actualizada",
        "detail": "Nuevos usuarios con posibilidad de instruir anexados a la lista de posibles instructores",
        "date": "15/04/2018"
      },
       {
        "action":"Curso de Centinela Pospuesto para aprobación",
        "detail": "Se pospuso la planificación realizada del curso de Centinela Avanzado",
        "date": "20/04/2018"
      }      
   ];

  

  $scope.openModalAction = function(size, device){
    // console.log(device.type);
    // console.log(angular.equals("undefined", "undefined"));
    if(!angular.isDefined(device.type)){
      $scope.openMicro(size, device);
    }else{
      $scope.open(size, device);
    }
  };

  $scope.open = function (size, device) {
    var modalInstance =   $uibModal.open({
      animation: false,
      controller: 'DisModalCtrl',
      templateUrl: 'views/actions/addActDis.html',
      scope: $scope,
      size: size,
      resolve: {
         device: function () {
           return device;
         }
      }
    });

    modalInstance.result.then(function () {
      // if(shift){
      //   if(index){
      //     angular.merge($scope.shifts[index], $scope.shifts[index], shift);
      //     angular.merge($scope.safeShifts[index], $scope.safeShifts[index], shift);
      //   }else{
      //     $scope.getShifts();
      //   }
      // }
    }, function (data) {
       if(data == "update"){
        $scope.getAll();
      }
    });
  };

  $scope.openMicro = function (size, device) {
    var modalInstance =   $uibModal.open({
      animation: false,
      controller: 'DisModalCtrl',
      templateUrl: 'views/actions/addActMic.html',
      scope: $scope,
      size: size,
      resolve: {
         device: function () {
           return device;
         }
      }
    });
    modalInstance.result.then(function () {
      // if(shift){
      //   if(index){
      //     angular.merge($scope.shifts[index], $scope.shifts[index], shift);
      //     angular.merge($scope.safeShifts[index], $scope.safeShifts[index], shift);
      //   }else{
      //     $scope.getShifts();
      //   }
      // }
    }, function (data) {
          if(data == "update"){
      $scope.getAll();
    }
  });
};
})

.controller('DisModalCtrl', function ($scope, $uibModal, $webSql, $uibModalInstance, $timeout, device) {

  $scope.db = $webSql.openDatabase('komodo', '1.0', 'Komodo DB', 2 * 1024 * 1024);
  $scope.showResult = false;
  $scope.selectedDevice = device;
  $scope.modalDevicesData = [];
  $scope.actionMicro = {
    selectedAction: '0',
    selectedActionDevices : '0'
  };
  $scope.action = {
    selectedCommand : '0'
  };

  $scope.ok = function () {
    if($scope.action.selectedCommand == 0){
      $scope.showResult = false;
      $scope.showResultLoad = false;
      $uibModalInstance.close();
    }else{
      $scope.showResult = true;
      $scope.showResultLoad = true;
      $timeout(function () {
        $scope.showResultLoad = false;

        if($scope.action.selectedCommand == 1){
          $scope.responseContent = `SNMPv2-MIB::sysDescr.0 = STRING: ILOM machine custom description
            SNMPv2-MIB::sysObjectID.0 = OID: SUN-ILOM-SMI-MIB::sunILOMSystems
            DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (16439826) 1 day, 21:39:58.26
            SNMPv2-MIB::sysContact.0 = STRING: set via snmp test
            SNMPv2-MIB::sysName.0 = STRING: SUNSPHOSTNAME
            SNMPv2-MIB::sysLocation.0 = STRING:
            SNMPv2-MIB::sysServices.0 = INTEGER: 72
            SNMPv2-MIB::sysORLastChange.0 = Timeticks: (14) 0:00:00.14
            SNMPv2-MIB::sysORID.1 = OID: IF-MIB::ifMIB
            SNMPv2-MIB::sysORID.2 = OID: SNMPv2-MIB::snmpMIB
            SNMPv2-MIB::sysORID.3 = OID: TCP-MIB::tcpMIB
            SNMPv2-MIB::sysORID.4 = OID: RFC1213-MIB::ip
            SNMPv2-MIB::sysORID.5 = OID: UDP-MIB::udpMIB
            SNMPv2-MIB::sysORID.6 = OID: SNMP-VIEW-BASED-ACM-MIB::vacmBasicGroup
            SNMPv2-MIB::sysORID.7 = OID: SNMP-FRAMEWORK-MIB::snmpFrameworkMIBCompliance
            SNMPv2-MIB::sysORID.8 = OID: SNMP-MPD-MIB::snmpMPDCompliance
            SNMPv2-MIB::sysORID.9 = OID: SNMP-USER-BASED-SM-MIB::usmMIBCompliance
            SNMPv2-MIB::sysORDescr.1 = STRING: The MIB module to describe generic objects for network interface sub-layers
            SNMPv2-MIB::sysORDescr.2 = STRING: The MIB module for SNMPv2 entities
            SNMPv2-MIB::sysORDescr.3 = STRING: The MIB module for managing TCP implementations
            SNMPv2-MIB::sysORDescr.4 = STRING: The MIB module for managing IP and ICMP implementations
            SNMPv2-MIB::sysORDescr.5 = STRING: The MIB module for managing UDP implementations
            SNMPv2-MIB::sysORDescr.6 = STRING: View-based Access Control Model for SNMP.
            SNMPv2-MIB::sysORDescr.7 = STRING: The SNMP Management Architecture MIB.
            SNMPv2-MIB::sysORDescr.8 = STRING: The MIB for Message Processing and Dispatching.
            SNMPv2-MIB::sysORDescr.9 = STRING: The management information definitions for the SNMP User-based Security Model.
            SNMPv2-MIB::sysORUpTime.1 = Timeticks: (1) 0:00:00.01
            SNMPv2-MIB::sysORUpTime.2 = Timeticks: (2) 0:00:00.02
            SNMPv2-MIB::sysORUpTime.3 = Timeticks: (2) 0:00:00.02
            SNMPv2-MIB::sysORUpTime.4 = Timeticks: (2) 0:00:00.02
            SNMPv2-MIB::sysORUpTime.5 = Timeticks: (2) 0:00:00.02
            SNMPv2-MIB::sysORUpTime.6 = Timeticks: (2) 0:00:00.02
            SNMPv2-MIB::sysORUpTime.7 = Timeticks: (14) 0:00:00.14
            SNMPv2-MIB::sysORUpTime.8 = Timeticks: (14) 0:00:00.14
            SNMPv2-MIB::sysORUpTime.9 = Timeticks: (14) 0:00:00.14`;

        }else if($scope.action.selectedCommand == 2){
          $scope.responseContent = `SNMPv2-MIB::sysName.0 = STRING: SUNSPHOSTNAME
            SNMPv2-MIB::sysObjectID.0 = OID: SUN-ILOM-SMI-MIB::sunILOMSystems
            SUN-ILOM-CONTROL-MIB::ilomCtrlDateAndTime.0 = STRING: 2007-12-10,20:33:32.0`;
        }else if($scope.action.selectedCommand == 3){
          $scope.responseContent = 'Sin respuesta del Dispositivo';
        }
      }, 700);
    }
  };
   $scope.cancel = function () {
    $uibModalInstance.close();
  };

  $scope.exec = function () {
    if($scope.actionMicro.selectedActionDevices == 0 || $scope.actionMicro.selectedAction == 0 ) return;
    $scope.showResult = true;
    $scope.showResultLoad = true;
    $timeout(function () {
      $scope.showResultLoad = false;
      var rand = Math.round(Math.random());
      console.log(rand);
      if(rand == 1){
        $scope.responseContent = 'Acción ejecutada correctamente';
      }else{
        $scope.responseContent = 'Acción no ejecutada';
      }
    }, 700);
  };

  $scope.getAllModal = function() {
    $scope.db.selectAll("devices").then(function(results) {
      for(var i=0; i < results.rows.length; i++){
        $scope.modalDevicesData.push(results.rows.item(i));
      }
    });
  };

  $scope.getCommand = function (id) {
    var valueRet;
    angular.forEach($scope.commandsData, function(value, key) {
      if(value.id == id){
        valueRet = value;
      }
    });
    return valueRet;
  };

  $scope.readCommands = function () {
    $scope.db.selectAll("commands").then(function(results) {
      $scope.commandsData = [];
      for(var i=0; i < results.rows.length; i++){
        $scope.commandsData.push(results.rows.item(i));
      }
    });
  };

  $scope.getDeviceComand = function () {
  $scope.db.selectAll("device_command").then(function(results) {
    $scope.deviceCommand = [];
    for(var i=0; i < results.rows.length; i++){
      $scope.deviceCommand.push(results.rows.item(i));
    }
  });
  };
});
