angular
.module('app.dispositives.list',[])
.controller('DispositivesCtrl', function ($scope, $uibModal, $webSql, toastr) {

  $scope.db = $webSql.openDatabase('komodo', '1.0', 'Komodo DB', 2 * 1024 * 1024);

  $scope.readDevices = function () {
    $scope.db.selectAll("devices").then(function(results) {
      $scope.devicesData = [];
      for(var i=0; i < results.rows.length; i++){
        $scope.devicesData.push(results.rows.item(i));
      }
    });
  };

  $scope.open = function (size, device) {
    var modalInstance =   $uibModal.open({
      animation: false,
      controller: 'DisModalCtrl',
      templateUrl: 'views/dispositives/addDispositive.html',
      scope: $scope,
      size: size,
       resolve: {
        device: function () {
          return {device: device};
        }
       }
    });
    modalInstance.result.then(function () {

    }, function (data) {
            if(data == "update"){
        $scope.readDevices();
      }
    });
  };

  $scope.delete = function () {
    var r = confirm('¿Realmente desea eliminar el dispositivo?');
  };

    $scope.delete = function (id) {
    var r= confirm('¿Realmente desea elimiar el usuario?');
    if(r){
      $scope.db.del("devices", {"id": id});
      $scope.readDevices();
    }
  };

})
.controller('DisModalCtrl', function ($uibModalInstance, $scope, $webSql, toastr, device) {

  $scope.toastr = toastr;
  $scope.deviceData = {};

  if(angular.isDefined(device.device.id)){
     angular.copy(device.device, $scope.deviceData);
  }else{
    $scope.new = true;
  }
  $scope.db = $webSql.openDatabase('komodo', '1.0', 'Komodo DB', 2 * 1024 * 1024);

  $scope.addDevice = function () {
    $scope.deviceData.identifier = $scope.deviceData.name+"-"+$scope.deviceData.brand;
    $scope.db.insert('devices', $scope.deviceData)
    .then(function(results) {
      console.log(results);
      $uibModalInstance.dismiss('update');
    },function (err) {
      if(err.code === 6){
        $scope.toastr.error("err");
      }
      console.log(err);
    });

  };

  $scope.readMicros = function () {
    $scope.db.selectAll("controllers").then(function(results) {
      $scope.microsData = [];
      for(var i=0; i < results.rows.length; i++){
        $scope.microsData.push(results.rows.item(i));
      }
    });
  };

  $scope.update = function () {
    $scope.db.update("devices", $scope.deviceData, { 'id' : ''+$scope.deviceData.id+'' });
    $uibModalInstance.dismiss('update');
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
