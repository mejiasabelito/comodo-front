// Default colors
var brandPrimary =  '#20a8d8';
var brandSuccess =  '#4dbd74';
var brandInfo =     '#63c2de';
var brandWarning =  '#f8cb00';
var brandDanger =   '#f86c6b';

var grayDark =      '#2a2c36';
var gray =          '#55595c';
var grayLight =     '#818a91';
var grayLighter =   '#d1d4d7';
var grayLightest =  '#f8f9fa';

angular
.module('app', [
  'ui.router',
  'ui.bootstrap',
  'ngAnimate',
  'ngSanitize',
  'oc.lazyLoad',
  'ncy-angular-breadcrumb',
  'angular-loading-bar',
  'angular-websql',
  'ngStorage',
  'smart-table',
  'toastr',
  'ngPatternRestrict'
])
.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
  cfpLoadingBarProvider.includeSpinner = false;
  cfpLoadingBarProvider.latencyThreshold = 1;
}])
.run(['$rootScope', '$state', '$stateParams', function($rootScope, $state, $stateParams) {
  $rootScope.$on('$stateChangeSuccess',function(){
    document.body.scrollTop = document.documentElement.scrollTop = 0;
  });
  $rootScope.$state = $state;
  return $rootScope.$stateParams = $stateParams;
}])
.run(function ($localStorage, $state, $transitions) {
    var storage = $localStorage;
    $transitions.onStart( { to: 'app.**' }, function(trans) {
      if(storage.login == !true || !angular.isDefined(storage.login)){
        return $state.target("appSimple.login");
      }
    });
})
.run(['$webSql','$localStorage','$http','$state', '$rootScope', function($webSql, $localStorage, $http, $state, $rootScope) {
  var storage = $localStorage;
  $rootScope.logout = function () {
    delete storage.login;
    delete storage.userType;
    $state.go("appSimple.login");
  }
  if(storage.init != true){
    var db = $webSql.openDatabase('komodo', '1.0', 'Komodo DB', 2 * 1024 * 1024);
    $http.get('src/data/tables.json')
      .then(function(response) {
        for(var table in response.data){
          db.createTable(table, response.data[table]);
        }
        storage.init = true;
        db.bulkInsert('users',
          [
            {"document": 'c', "nationality": 'v', 'ci': "12345678",  "first_name":"Admin", "last_name":"Example", "password":"12345678", "position":"Admin", "type":"admin"},
            {"document": 'c', "nationality": 'v', 'ci': "87654321",  "first_name":"Consultor", "last_name":"Example", "password":"12345678", "position":"Consultor", "type":"employee"},
          ]
          ).then(function(results) {
            console.log(results.insertId);
        },function (err) {
          console.log(err);
        })
      }, function (err) {
        console.log(err);
      })
  }else{
      $rootScope.userType = storage.userType;
  }

}]);
